INSERT INTO vulnerability(score, deleted, title, product,severity)
VALUES('2', false, 'Vuln-1', 'Laravel','Low');
INSERT INTO vulnerability(score, deleted, title, product,severity)
VALUES('5', false, 'Vuln-2', 'Laravel','Low');
INSERT INTO vulnerability(score, deleted, title, product,severity)
VALUES('6', true, 'Vuln-3', 'Laravel','Medium');
INSERT INTO vulnerability(score, deleted, title, product,severity)
VALUES('8', false, 'Vuln-4', 'Laravel','High');
INSERT INTO vulnerability(score, deleted, title, product,severity)
VALUES('10', false, 'Vuln-5', 'Laravel','High');